
<!-- connection BDD -->

<?php  
try {
$bdd = new PDO('mysql:host=localhost;dbname=phpblog;charset=utf8', 'lucilelison', 'm1HKNMTT!');
} 
    catch(Exception $e) 
    {       die('Erreur : '.$e->getMessage()); }
?>




<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Blog !</title>
	<link rel="stylesheet" type="text/css" href="dist/css/main.min.css">
</head>
<body>

	<header>

		<form class="login" action="connexion.php" method="post">
			
			<input class="btn_login" type="submit" name="connexion" value="Connexion">
			

			<!-- <input value="User Name" type="text" name="User Name">
			<input value="Passworld" type="text" name="Passworld">
			<button class="btn_login" href="#">Login</button> -->
		</form>
		<div class="titre_principal">
			<h1>Blog From Scratch</h1>
		</div>
	</header>

	

		<main>

			<form class="filtre" method="get" action="index.php">


				<select id="ListeElement" onchange="VerifListe();"> 
					
					<option value="valeur0" selected="selected">Choisissez un Auteur...</option>

				  	<option value="<php id=[1]?>">Auteur 1</option> 
				  	<option value="<php id=[2]?>">Auteur 2</option> 
				  	<option value="<php id=[3]?>">Auteur 3</option>
				  	<option value="<php id=[4]?>">Auteur 4</option>
				  	<option value="<php id=[5]?>">Auteur 5</option>
				  	<option value="<php id=[6]?>">Auteur 6</option>
				  	<option value="<php id=[7]?>">Auteur 7</option>
				  	<option value="<php id=[8]?>">Auteur 8</option>
				  	<option value="<php id=[9]?>">Auteur 9</option> 
				
			

					<input type="submit" name="filtre" value="Get Selected Values" />
					
					<?php
						if(isset($_GET['filtre'])){
						$selected_val = $bdd->query ('SELECT * FROM articles WHERE author='['?']);
						$filtre = $bdd->prepare($selected_val);
						$filtre->execute($_GET['filtre']);
						
						echo "You have selected :" .$selected_val;  // Displaying Selected Value
						}
					?>

				</select> 


			</form>

			<div class="bloc_articles">

				<?php 
				$articles = $bdd->query ('SELECT * FROM articles');
				while ($row = $articles->fetch()){
				?>

				<article class="article">

			        <a href="articles.php?id=<?php echo $row['id'];?> ">

					<div class="img">
						<img src="<?php echo $row['image'];?>">
					</div>
					<div class="titre_art">
						<h3><?php echo $row['title']?></h3>
					</div>
					<div class="extract">
						<p><?php echo $row ['extract']?></p>
					</div>
					<div class="publish_date">
						<p><?php echo $row ['publish_date']?></p>
					</div>
					<div class="author">
						<p>Auteur : <?php echo $row ['author']?></p>
					</div>

				</a>

				</article>
					<?php  
					} 
					?>

			</div>

		</main>

	

</body>
</html>
