

<?php  
session_start();

try {
$bdd = new PDO('mysql:host=localhost;dbname=phpblog;charset=utf8', 'lucilelison', 'm1HKNMTT!');
} 
    catch(Exception $e) 
    {       die('Erreur : '.$e->getMessage()); }

$articlesperso = $bdd->query('SELECT articles.*, users.username FROM articles INNER JOIN users ON articles.author=users.id AND users.id=' . $_SESSION['id']);

?>

<!DOCTYPE html>
<html lang="en">

   <head>
      <title>page membre</title>
      <meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="dist/css/main_pagemembre.min.css">

   </head>


   <body>
      <form method="POST" action="connexion.php">
         <a href=""></a>

         <input class="btn_deconnexion" type="submit" name="formdeconnexion" value="Se déconnecter !" />
      </form>
         
<?php
   while ($rowperso = $articlesperso->fetch()) {
?>
      <div >
         <h2>Profil de <?php echo $rowperso['username']; ?></h2>
         <br /><br />
       
         
      </div>


      <div class="bloc_article">
         <article class="article">
            <div class="img">
               <img src="<?php echo $rowperso['image'];?>">
            </div>
            <div class="titre_art">
               <h3>
                  <?php echo $rowperso['title']?>
               </h3>
            </div>
            
            <div class="infos_art">
               <p><?php echo $rowperso ['publish_date']?></p>
               <p>Auteur : <?php echo $rowperso ['username']?></p>
            </div>
            <div class="content">
               <p><?php echo $rowperso['content']?></p>
            </div>
         </article>
      </div>

<?php
}
?>
   </body>
</html>
